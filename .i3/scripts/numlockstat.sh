#!/bin/bash

STAT=`xset q|grep LED|awk '{print $10}'`

case $STAT in
	00000000*)
		printf 'OFF\n'
	;;
	00001005*)
		printf 'OFF\n'
	;;
	00000002*)
		printf 'ON\n'
	;;
	00001007*)
		printf 'ON\n'
	;;
	*)
		printf '?\n'
	;;
esac

