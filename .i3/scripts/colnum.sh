#!/bin/bash

R='\e[0;31m'
Y='\e[0;33m'
G='\e[0;32m'
C='\e[0m'

STAT=`xset q|grep LED|awk '{print $10}'`

case $STAT in
	00000000*)
		echo "<span color='#ff0000'> OFF</span>"
	;;
	00001005*)
		echo "<span color='#ff0000'> OFF</span>"
	;;
	00000002*)
		echo "<span color='#00ff00'> ON </span>"
	;;
	00001007*)
		echo "<span color='#00ff00'> ON </span>"
	;;
	*)
		echo "<span color='#ffff00'> ?? </span>"
	;;
esac

