#!/bin/bash

shopt -s nullglob

LOC=~/.wallpaper
LDMBG=/var/local/wall/wall
cd $LOC

while true; do
	file=`/bin/ls -1 "$LOC" | sort --random-sort | head -1`
	path=`readlink --canonicalize "$LOC/$file"`
	feh --bg-scale "$path"
	unlink $LDMBG
	ln -s "$path" "$LDMBG"

	sleep 15m
done

