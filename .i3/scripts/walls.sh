#!/bin/bash

shopt -s nullglob

LOC=~/.wallpaper
cd $LOC

#if [ -d $LOC ] then
	while true; do
		files=()
		for i in *.jpg *.png; do
			[[ -f $i ]] && files+=("$i")
		done
		range=${#files[@]}

		((range)) && feh --bg-scale "${files[RANDOM % range]}"

		sleep 15m
	done
#else
#	echo Fuck off

