##Common settings

set $mod Mod4
workspace_layout tabbed
font pango:keyrus 14
floating_modifier $mod
focus_follows_mouse yes
workspace_auto_back_and_forth yes
new_window normal
new_float pixel 3
hide_edge_borders both

##Keymapping
bindsym $mod+Return exec urxvt

bindsym $mod+Shift+Q kill

bindsym $mod+d exec i3-dmenu-desktop

bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

bindsym $mod+h split h

bindsym $mod+v split v

bindsym $mod+f fullscreen

bindsym $mod+s layout stacking
bindsym $mod+t layout tabbed
bindsym $mod+e layout default

bindsym $mod+Shift+space floating toggle

bindsym $mod+space focus mode_toggle

bindsym $mod+a focus parent

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+exclam move container to workspace 1
bindsym $mod+Shift+at move container to workspace 2
bindsym $mod+Shift+numbersign move container to workspace 3
bindsym $mod+Shift+dollar move container to workspace 4
bindsym $mod+Shift+percent move container to workspace 5
bindsym $mod+Shift+asciicircum move container to workspace 6
bindsym $mod+Shift+ampersand move container to workspace 7
bindsym $mod+Shift+asterisk move container to workspace 8
bindsym $mod+Shift+parenleft move container to workspace 9
bindsym $mod+Shift+parenright move container to workspace 10

bindsym $mod+Shift+C reload
bindsym $mod+Shift+R restart
bindsym $mod+Shift+E exit

# Make the currently focused window a scratchpad
bindsym $mod+Shift+minus move scratchpad
# Show the first scratchpad window
bindsym $mod+minus scratchpad show

#Save a screenshot
bindsym Print exec --no-startup-id ~/.i3/scripts/shoot
bindsym Mod1+Print exec --no-startup-id ~/.i3/scripts/rshoot

#PCManFM
bindsym $mod+r exec --no-startup-id "pcmanfm"

#Volume management
bindsym XF86AudioRaiseVolume exec --no-startup-id amixer -q set Master 5%+ unmute
bindsym XF86AudioLowerVolume exec --no-startup-id amixer -q set Master 5%- unmute
bindsym XF86AudioMute exec --no-startup-id amixer -q set Master toggle

bar {
	#status_command i3status -c ~/.i3/i3status.conf
	status_command i3blocks
	font xft:keyrus 14
	tray_output eDP1

	colors {
		background #000000
		#statusline #ffffff

		focused_workspace  #A22814 #A22814 #ffffff
		active_workspace   #52130A #52130A #ffffff
		inactive_workspace #555555 #222222 #888888
		urgent_workspace   #146CA3 #146CA3 #ffffff
	}
}

## Window behaviour
for_window [class="URxvt"] border none
for_window [class="terminology"] border none

##Workspace assignations
workspace 9 output HDMI1

assign [class="URxvt"] 9
assign [class="Pidgin"] 2
assign [class="Skype"] 2

for_window [class="Wine"] floating enable
for_window [class="Oblogout"] fullscreen true
for_window [class="Oblogout"] border none
for_window [title="^Terraria*"] floating enable

##Colorization
# class                 border  backgr. text    indicator
client.focused          #A22814 #A22814 #ffffff #2e9ef4
client.focused_inactive #52130A #52130A #ffffff #484e50
client.unfocused        #555555 #222222 #888888 #292d2e
client.urgent           #146CA3 #146CA3 #ffffff #900000

exec ~/.i3/startup

